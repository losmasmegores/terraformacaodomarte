package src;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import Comparators.ComparatorBosque;
import Comparators.ComparatorCiutat;
import Comparators.ComparatorVictoria;
import DAO.DAOCorporations;
import DAO.DAOGames;
import DAO.DAOGamesWins;
import DAO.DAOMakers;
import DAO.DAOPlayer;
import Models.Corporations;
import Models.Dices;
import Models.EnumCorporations;
import Models.Games;
import Models.GamesWins;
import Models.Makers;
import Models.Players;

public class Main {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Random r = new Random();

		//--------------------DAOS-----------------
		DAOGames gamesDAO = new DAOGames();
		DAOCorporations corporationsDAO = new DAOCorporations();
		DAOPlayer playerDAO = new DAOPlayer();
		DAOMakers makersDAO = new DAOMakers();
		DAOGamesWins gwDAO = new DAOGamesWins();
		//-----------------------------------------


		for (int i = 0; i < 6; i++) {
			makersDAO.generateMakers(i+"", 3);
		}
		
		for (int i = 0; i < 24; i++) {
			makersDAO.generateMakers(i+"", 4);

		}
		
		for (int i = 0; i < 37; i++) {
			makersDAO.generateMakers(i+"", 6);
		}
		
		List<Makers> ESQUINAS = makersDAO.findAllWhere("Where maxNeighbours = 3");
		for (Makers makers : ESQUINAS) {
			List<Makers> aristas = makersDAO.findAllWhere("Where maxNeighbours = 4");
			System.out.println(aristas);
			List<Makers> resto = makersDAO.findAllWhere("Where maxNeighbours = 6");
			System.out.println(resto);
			List<Makers> ari_res = new ArrayList<>();
			
			
			for (int i = 0; i < 2; i++) {
				int ran = r.nextInt(aristas.size());
				while (aristas.get(ran).getVecinos().size()>0) {
					ran = r.nextInt(aristas.size());
				}
				 if (!makers.getVecinos().contains(aristas.get(ran))) {
					  Makers nuevo = aristas.get(ran);
			            makers.getVecinos().add(nuevo);
			            nuevo.getVecinos().add(makers);
			            
						 makersDAO.update(nuevo);

			            ari_res.add(aristas.get(ran));
			        }
			}
			int ran = r.nextInt(resto.size());
			while (resto.get(ran).getVecinos().size()>0) {
				ran = r.nextInt(resto.size());
			}
			  if (!makers.getVecinos().contains(resto.get(ran))) {
				  Makers nuevo = resto.get(ran);
		            makers.getVecinos().add(nuevo);
		            nuevo.getVecinos().add(makers);
		            
		            for (int i = 0; i < ari_res.size(); i++) {
		            	
		            	Makers arires = ari_res.get(i);
		            	nuevo.getVecinos().add(arires);
		            	arires.getVecinos().add(nuevo);
					}
					  makersDAO.update(nuevo);
		        }
			  makersDAO.update(makers);
		}
		List<Corporations> corps = Arrays.asList(
				new Corporations(EnumCorporations.CREDICOR, "daawd",0),
				new Corporations(EnumCorporations.ECOLINE, "daawd",0),
				new Corporations(EnumCorporations.HELION, "daawd",0),
				new Corporations(EnumCorporations.MINING_GUILD, "daawd",0),
				new Corporations(EnumCorporations.PHOBLOG, "daawd",0),
				new Corporations(EnumCorporations.THARSIS_REPUBLIC, "daawd",0),
				new Corporations(EnumCorporations.THORGATE, "daawd",0),
				new Corporations(EnumCorporations.U_N_MARS_INITIATIVE, "daawd",0)
				);
		for (int i = 0; i < corps.size(); i++) {
			corporationsDAO.generateCorporation(corps.get(i));
		}
		Games game = gamesDAO.generateAndgetGames(0, 0, 0, Date.valueOf(LocalDate.now()), null);
		System.out.println("Cuants Jugadors vols?");
		int n = sc.nextInt();
		for (int i = 0; i < n; i++) {
			System.out.print("Escriu el teu nom: ");
			sc.next();
			String nom = sc.nextLine();
			corps = corporationsDAO.getCorpWithOutPlayers();
			Corporations c = corps.get(r.nextInt(corps.size()));
			while(c.getPlayers() != null) {
				c = corporationsDAO.getCorpWithOutPlayers().get(r.nextInt(corporationsDAO.getCorpWithOutPlayers().size()));
			}
			Players a = new Players(nom);
			playerDAO.save(a);
			a.setCorporations(c);
			playerDAO.update(a);
			corporationsDAO.update(c);
			game.addJugadors(a);
		}
		gamesDAO.update(game);

		
		while(!game.isEndedGame()) {
			corps = corporationsDAO.getCorpWithPlayers();
			for (int i = 0; i < corps.size(); i++) {
				game.resolveDices(new Dices(corps.get(i),game.rollingDices()));	
			}
//			game.isEndedGame();
		}
		
		corps = corporationsDAO.getCorpWithPlayers();
		for (Corporations c : corps) {
//		    Hibernate.initialize(c.getMakers());
		    corporationsDAO.addVictoriaByMarkers(c.getId());
		}

		corps.sort(new ComparatorCiutat());
		corporationsDAO.addVictoria(corps.get(0).getId(), 5);
		corporationsDAO.addVictoria(corps.get(1).getId(), 3);
		corps.sort(new ComparatorBosque());
		corporationsDAO.addVictoria(corps.get(0).getId(), 5);
		corporationsDAO.addVictoria(corps.get(1).getId(), 3);
		
		corps.sort(new ComparatorVictoria());
//		gamesDAO.update(game);
		GamesWins gg = gwDAO.generateAndgetGames(game, corps.get(0).getPlayers());
		playerDAO.addGameWin(corps.get(0).getPlayers().getId(),gg);
		sc.close();
		
	}


}

