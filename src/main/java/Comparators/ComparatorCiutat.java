package Comparators;

import java.util.Comparator;
import java.util.List;

import DAO.DAOMakers;
import Models.Corporations;
import Models.Makers;
import Models.TypeMaker;

public class ComparatorCiutat implements Comparator<Corporations>{

	@Override
	public int compare(Corporations o1, Corporations o2) {
		DAOMakers makersDAO = new DAOMakers();
		List<Makers> ciudadesO1 = makersDAO.getMakersByTypeCorporation(o1, TypeMaker.CIUTAT);
		List<Makers> ciudadesO2 = makersDAO.getMakersByTypeCorporation(o2, TypeMaker.CIUTAT);
		if(ciudadesO1.size() > ciudadesO2.size())return 1;
		else if(ciudadesO1.size() == ciudadesO2.size()) {
			if(o1.getId() > o2.getId())return -1;
			else return 1;
		}
		else return -1;
	}

}
