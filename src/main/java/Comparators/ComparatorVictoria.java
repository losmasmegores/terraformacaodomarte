package Comparators;

import java.util.Comparator;
import Models.Corporations;

public class ComparatorVictoria implements Comparator<Corporations>{

	@Override
	public int compare(Corporations o1, Corporations o2) {
		if(o1.getVicPoints() > o2.getVicPoints())return -1;
		else return 1;
	}

}
