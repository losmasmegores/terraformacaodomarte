package Models;

import java.util.HashMap;

public class Dices {
	private Corporations corp;
	private HashMap<Integer, Integer> resultTirades;
	
	public Dices(Corporations id, HashMap<Integer,Integer> tirades) {
		this.corp = id;
		this.resultTirades = tirades;
	}
	public Dices(HashMap<Integer,Integer> tirades) {
		this.resultTirades = tirades;
	}

	public Corporations getIdCorporation() {
		return corp;
	}

	public void setIdCorporation(Corporations idCorporation) {
		this.corp = idCorporation;
	}

	public HashMap<Integer, Integer> getResultTirades() {
		return resultTirades;
	}

	public void setResultTirades(HashMap<Integer, Integer> resultTirades) {
		this.resultTirades = resultTirades;
	}
	
	
}
