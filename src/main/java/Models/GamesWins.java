package Models;

import javax.persistence.CascadeType;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "gamesWins")
public class GamesWins {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idGamesWins")
    private int id;
	
	@OneToOne(cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "idGame")
    private Games games;

    @ManyToOne
    @JoinColumn(name = "idPlayer")
    private Players players;

    
    //CONSTRUCTORS
    
    public GamesWins() {
		super();
	}


	public GamesWins(Games games, Players players) {
		super();
		this.games = games;
		this.players = players;
	}  
    
    
    //GETTERS && SETTERS
	
	public Games getGames() {
		return games;
	}

	public void setGames(Games games) {
		this.games = games;
	}


	
	public Players getPlayers() {
		return players;
	}

	public void setPlayers(Players players) {
		this.players = players;
	}


	
	public int getId() {
		return id;
	}  
}
