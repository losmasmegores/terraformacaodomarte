package Models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "corporations")
public class Corporations{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idCorporations")
	private int id;
	
	@Enumerated(EnumType.STRING)@Column(name = "name", length = 50, nullable = false)
	private EnumCorporations name;
	
	@Column(name = "description", length = 50, nullable = false)
	private String description;
	
	@Column(name = "victoryPoints")
	private int vicPoints = 0;
	
	@OneToOne(mappedBy = "corporations")
    private Players players;
	
	@OneToMany(fetch = FetchType.EAGER,mappedBy = "corporation")
	private Set<Makers> makers = new HashSet<Makers>();

	
	//CONSTRUCTORS
	
	public Corporations() {
		super();
	}
	
	
	
	public Corporations(EnumCorporations name, String description, int vicPoints) {
		super();
		this.name = name;
		this.description = description;
		this.vicPoints = vicPoints;
	}

	

	public Corporations(EnumCorporations name, String description, int vicPoints, Players players, Set<Makers> makers) {
		super();
		this.name = name;
		this.description = description;
		this.vicPoints = vicPoints;
		this.players = players;
		this.makers = makers;
	}



	//GETTERS && SETTERS
	public EnumCorporations getName() {
		return name;
	}

	public void setName(EnumCorporations name) {
		this.name = name;
	}
	
	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	

	public int getVicPoints() {
		return vicPoints;
	}

	public void setVicPoints(int vicPoints) {
		this.vicPoints = vicPoints;
	}

	
	
	public Players getPlayers() {
		return players;
	}

	public void setPlayers(Players players) {
		this.players = players;
	}
	
	

	public Set<Makers> getMakers() {
		return makers;
	}

	public void setMakers(Set<Makers> makers) {
		this.makers = makers;
	}
	
	public void addMakers(Makers maker) {
		this.makers.add(maker);
	}
	

	
	public int getId() {
		return id;
	}

}
