package Models;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import DAO.DAOCorporations;
import DAO.DAOGames;
import DAO.DAOMakers;


@Entity
@Table(name = "games")
public class Games {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idGame")
	private int id;

	@Column(name = "oxygen")
	private int oxy = 0;
	
	@Column(name = "temperature")
	private double temp = -30;
	
	@Column(name = "oceans")
	private int oceans = 0;
	
	@Column(name = "dateStart")
	private Date dateStart;
	
	@Column(name = "dateEnd")
	private Date dateEnd;
	
	@OneToOne(mappedBy = "games", cascade = CascadeType.PERSIST)
    private GamesWins gamesWins;
	
	@ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.MERGE) 
	@JoinTable(
			name="Partidas", 
			joinColumns = @JoinColumn(name="id_Partida"), 
			inverseJoinColumns = @JoinColumn(name="id_Jugador")
	)
	private Set<Players> jugadors = new HashSet<Players>();
	
	//CONSTRUCTORS
	
	public Games() {
		super();
	}

	
	
	public Games(int oxy, double temp, int oceans, Date dateStart, Date dateEnd) {
		super();
		this.oxy = oxy;
		this.temp = temp;
		this.oceans = oceans;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
	}



	public Games(int oxy, double temp, int oceans, Date dateStart, Date dateEnd, GamesWins gamesWins,Set<Players> jugadors) {
		super();
		this.oxy = oxy;
		this.temp = temp;
		this.oceans = oceans;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.gamesWins = gamesWins;
		this.jugadors = jugadors;
	}

	
	public HashMap<Integer, Integer> rollingDices() {
		Random r = new Random();
		HashMap<Integer,Integer> Dices = new HashMap<Integer,Integer>();
		for(int i = 0; i < 6; i++) {
			int n = r.nextInt(6)+1;
			Dices.put(i+1, n);
		}
		return Dices;
	}
	
	public void resolveDices(Dices dados) {
		Scanner sc = new Scanner(System.in);

		DAOGames gamesDAO = new DAOGames();
		DAOCorporations corpDAO = new DAOCorporations();
		for (Entry<Integer, Integer> entry : dados.getResultTirades().entrySet()) {
			if(entry.getValue() >= 3) {
				switch (entry.getKey()) {
				case 1:
					System.out.println("SE AÑADE TEMPERATURA");
					gamesDAO.addTemperatura(this.getId(), 2);
					break;
				case 2:
					System.out.println("SE AÑADE OXYGENO");
					gamesDAO.addOxygen(this.getId(), 1);
					break;
				case 3:
					System.out.println("SE LE AÑADE UNA CASILLA DE OCEANO A LA CORPORACION");
					if(dados.getIdCorporation() != null)corpDAO.addMakerByType(TypeMaker.OCEA,dados.getIdCorporation().getId());
					break;
				case 4:
					System.out.println("SE LE AÑADE UNA CASILLA DE BOSQUE A LA CORPORACION");
					if(entry.getValue() >= 4 && dados.getIdCorporation() != null) corpDAO.addMakerByType(TypeMaker.BOSC,dados.getIdCorporation().getId());
					break;
				case 5:
					System.out.println("SE LE AÑADE UNA CASILLA DE CIUDAD A LA CORPORACION");
					if(dados.getIdCorporation() != null)corpDAO.addMakerByType(TypeMaker.CIUTAT,dados.getIdCorporation().getId());
					break;
				case 6:
					System.out.println("SE LE AÑADE 2 PUNTOS DE VICTORIA A LA CORPORACION");
					if(dados.getIdCorporation() != null)corpDAO.addVictoria(dados.getIdCorporation().getId(), 2);
					break;
				default:
					break;
				}
			}
			sc.nextLine();

		}
	}
	public boolean isEndedGame() {
		Scanner sc = new Scanner(System.in);
		DAOMakers makersDAO = new DAOMakers();
		DAOGames gamesDAO = new DAOGames();
		Games a = gamesDAO.find(this.getId());
		int condicions = 0;
		System.out.println(this.getTemp()+"TEMP");
		if(a.getTemp() > 0.1) {
			condicions++;
		}
		if(a.getOxy() > 13) {
			System.out.println("AADFAWFJIKAIOFJIOSEJUFIOAWEJFIOJAWFIOJAIFJIAJKIFJKAWLEJFKAJWÑFJAIOFJIOAHJFHIO");
			condicions++;
		}
		if(makersDAO.getMakersByTypeCorporation(null,TypeMaker.OCEA).size() < 1) {
			condicions++;
		}
		if(condicions > 1) {
			System.out.println("return true");
			return true;
		}
		else {
			System.out.println(condicions+" oxy: "+this.getOxy()+" temp "+this.getTemp());
			//sc.nextLine();

			return false;
		}
	}
	
	
	//GETTES && SETTERS
	
	public int getOxy() {
		return oxy;
	}

	public void setOxy(int oxy) {
		this.oxy = oxy;
	}



	public double getTemp() {
		return temp;
	}

	public void setTemp(double temp) {
		this.temp = temp;
	}



	public int getOceans() {
		return oceans;
	}

	public void setOceans(int oceans) {
		this.oceans = oceans;
	}



	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}



	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}



	public GamesWins getGamesWins() {
		return gamesWins;
	}

	public void setGamesWins(GamesWins gamesWins) {
		this.gamesWins = gamesWins;
	}


	
	public Set<Players> getJugadors() {
		return jugadors;
	}
	
	public void setJugadors(Set<Players> jugadors) {
		this.jugadors = jugadors;
	}

	public void addJugadors(Players jugadors) {
		this.jugadors.add(jugadors);
	}



	public int getId() {
		return id;
	}
}
