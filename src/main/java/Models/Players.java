package Models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "players")
public class Players {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idPlayer")
	private int id;

	@Column(name = "name", length = 50, nullable = false)
	private String name;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "players")
//	@Column(name = "wins")
	private Set<GamesWins> wins = new HashSet<GamesWins>();

	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	@JoinColumn(name = "idCorporations",nullable = true)
	private Corporations corporations;
	
	@ManyToMany(mappedBy = "jugadors")
	private Set<Games> juegos = new HashSet<Games>();
	
	//CONSTRUCTORS
	
	public Players() {
		super();
	}

	public Players(String name) {
		super();
		this.name = name;
	}
	public Players(String name, Corporations corporations) {
		super();
		this.name = name;
		this.wins = new HashSet<GamesWins>();
		this.corporations = corporations;
		this.juegos = new HashSet<Games>();
	}
	public Players(String name, Set<GamesWins> wins, Corporations corporations, Set<Games> juegos) {
		super();
		this.name = name;
		this.wins = wins;
		this.corporations = corporations;
		this.juegos = juegos;
	}

		
	//GETTERS && SETTERS
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	
	public Set<GamesWins> getWins() {
		return wins;
	}

	public void setWins(Set<GamesWins> wins) {
		this.wins = wins;
	}
	public void addWins(GamesWins win) {
		this.wins.add(win);
	}
	
	

	public Corporations getCorporations() {
		return corporations;
	}

	public void setCorporations(Corporations corporations) {
		this.corporations = corporations;
	}
	
	

	public Set<Games> getJuegos() {
		return juegos;
	}

	public void setJuegos(Set<Games> juegos) {
		this.juegos = juegos;
	}
	public void addJuegos(Games juego) {
		this.juegos.add(juego);
	}
	
	

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Players [id=" + id + ", name=" + name + ", wins=" + wins + ", corporations=" + corporations
				+ ", juegos=" + juegos + "]";
	}
	
}
