package DAO;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import Models.Dices;
import Models.Games;
import Models.Makers;
import Models.TypeMaker;

public class DAOGames extends DAOGeneric<Games, Integer> {
	public DAOGames() {
		super(Games.class);
	}

	public void generateGames(int oxy, double temp, int oceans, Date dateStart, Date dateEnd) {
		Games g = new Games(oxy, temp, oceans, dateStart, dateEnd);
		this.save(g);
	}
	public Games generateAndgetGames(int oxy, double temp, int oceans, Date dateStart, Date dateEnd) {
		Games g = new Games(oxy, temp, oceans, dateStart, dateEnd);
		this.save(g);
		return this.find(g.getId());
	}
	public void finishGame(int id,Date dateEnd) {
		Games g = this.find(id);
		g.setDateEnd(dateEnd);
		this.update(g);
	}
	public void addTemperatura(int id,double temp) {
		Games g = this.find(id);
		g.setTemp(g.getTemp()+temp);
		System.out.println(g.getTemp()+" TEMP");
		this.update(g);
	}
	public void restTemperatura(int id,double temp) {
		Games g = this.find(id);
		g.setTemp(g.getTemp()-temp);
		this.update(g);
	}
	public void addOxygen(int id,int oxy) {
		Games g = this.find(id);
		g.setOxy(g.getOxy()+oxy);
		System.out.println(g.getOxy()+" OXY");
		this.update(g);
	}
	public void restOxygen(int id,int oxy) {
		Games g = this.find(id);
		g.setOxy(g.getOxy()-oxy);
		this.update(g);
	}	
	public Games getGames(int id) {
		return this.find(id);
	}
}