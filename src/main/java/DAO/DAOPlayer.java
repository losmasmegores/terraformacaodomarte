package DAO;

import java.util.List;

import org.hibernate.Session;

import Models.Corporations;
import Models.Games;
import Models.GamesWins;
import Models.Players;

public class DAOPlayer extends DAOGeneric<Players, Integer> {
	public DAOPlayer() {
		super(Players.class);
	}

	public void generatePlayer(String name,Corporations c) {
		Players p = new Players(name,c);
		this.save(p);
	}

	public void setWinnerGame() {
		List<Players> p = findAll();
		Players guanyador = new Players();
		for (Players players : p) {
			if (players.getCorporations().getVicPoints() > guanyador.getCorporations().getVicPoints()) {
				guanyador = players;
			}

		}
		GamesWins victory = new GamesWins((Games) guanyador.getJuegos(), guanyador);
		guanyador.addWins(victory);
		this.update(guanyador);
	}
	public void addGame(int id,Games g) {
		Players p = this.find(id);
		p.addJuegos(g);
		this.update(p);
	}
	public void addGameWin(int id,GamesWins g) {
		Players p = this.find(id);
		p.addWins(g);
		this.update(p);
	}
	public List<Players> findByGame(Games g) {
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		List<Players> lista = session
				.createQuery("SELECT m FROM players m WHERE m.players IS NULL").getResultList();
		session.getTransaction().commit();
		return lista;
	}
	public List<Players> getPlayersWithoutCorp() {
		List<Players> corporacionesSinJugadores = this.findAllWhere("c WHERE c.corporations IS NULL");
		return corporacionesSinJugadores;
	}
}
