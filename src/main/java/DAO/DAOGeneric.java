package DAO;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;


public class DAOGeneric <T, ID extends Serializable> implements IDAOGeneric<T, ID>
{
	Class<T> entityClass;
	
	public DAOGeneric(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	public void save(T entity)
	{
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		session.persist(entity);
		session.getTransaction().commit();
	}
	
	public void delete(T entity)
	{
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		session.remove(entity);
		session.getTransaction().commit();
	}
	
	@Override
	public void delete(ID id)
	{
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		T entityToDelete = session.find(entityClass, id);
		session.remove(entityToDelete);
		session.getTransaction().commit();
	}
	
	public void update(T entity)
	{
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();	
		session.merge(entity);
		session.getTransaction().commit();
	}
	
	public List<T> findAll()
	{
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		List<T> registres = session.createQuery("FROM "+entityClass.getName()).getResultList();
		session.getTransaction().commit();
		return registres;
	}
	public List<T> findAllWhere(String where)
	{
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		List<T> registres = session.createQuery("FROM "+entityClass.getName()+" "+where).getResultList();
		session.getTransaction().commit();
		return registres;
	}
	
	@Override
	public T find(ID id)
	{
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		T entity = session.find(entityClass, id);
		session.getTransaction().commit();
		return entity;
	}

}
