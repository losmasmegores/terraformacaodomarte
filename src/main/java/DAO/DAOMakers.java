package DAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import Models.Corporations;
import Models.Makers;
import Models.TypeMaker;

public class DAOMakers extends DAOGeneric<Makers, Integer> {
	public DAOMakers() {
		super(Makers.class);
	}

	public void generateMakers(String name, int maxNeigh) {
		Makers m = new Makers(name, maxNeigh);
		this.save(m);
	}

	public List<Makers> getMakersByType(TypeMaker typemaker) {
		List<Makers> list = findAll();
		List<Makers> listaBuena = new ArrayList<Makers>();
		for (Makers makers : list) {
			if (makers.getTypeMaker().equals(typemaker)) {
				listaBuena.add(makers);
			}
		}
		return listaBuena;
	}

	public void addMakerPossibleNeightbours(Makers m1, Makers m2) {
		if (!m1.getVecinos().contains(m2) && !m1.equals(m2) && m1.getVecinos().size() < m1.getMaxNeigh()
				&& m2.getVecinos().size() < m2.getMaxNeigh()) {
			m1.addVecinos(m2);
		}
		this.update(m1);
	}

	public List<Makers> getMakersByTypeNoCorporation(TypeMaker t) {
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		List<Makers> lista = session
				.createQuery("SELECT m FROM Makers m WHERE m.typeMaker = :typeMaker AND m.corporation IS NULL")
				.setParameter("typeMaker", t).getResultList();
		session.getTransaction().commit();
		return lista;
	}

	public List<Makers> getMakersByTypeCorporation(Corporations c, TypeMaker t) {
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		List<Makers> lista = session
				.createQuery("FROM Makers m WHERE m.typeMaker = :typeMaker AND m.corporation = :Corporation")
				.setParameter("typeMaker", t).setParameter("Corporation", c).getResultList();
		session.getTransaction().commit();
		return lista;
	}

}