package DAO;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import Models.Corporations;
import Models.EnumCorporations;
import Models.Makers;
import Models.TypeMaker;

public class DAOCorporations extends DAOGeneric<Corporations, Integer> {
	public DAOCorporations() {
		super(Corporations.class);
	}

	public void generateCorporation(EnumCorporations name, String desc) {
		Corporations c = new Corporations(name, desc, 0);
		this.save(c);
	}
	public void generateCorporation(Corporations c) {
		this.save(c);
	}
	
	public void addVictoria(int id, int vic) {
		Corporations c = this.find(id);
		c.setVicPoints(c.getVicPoints()+vic);
		this.update(c);
	}
	public void addVictoriaByMarkers(int id) {
		Corporations c = this.find(id);
		if (c.getMakers() != null) {
		    for (Makers maker : c.getMakers()) {
		        c.setVicPoints(c.getVicPoints() + 1);
		    }
		}
		this.update(c);

	}
	public void addVictoriaByOceans(int id) {
		Corporations c = this.find(id);
		DAOMakers makersDAO = new DAOMakers();
		if(makersDAO.getMakersByTypeCorporation(c, TypeMaker.OCEA).size() > 2)c.setVicPoints(c.getVicPoints()+3);
		this.update(c);
	}
	public void addMaker(int id, Makers maker) {
		Corporations c = this.find(id);
		c.addMakers(maker);
		this.update(c);
	}
	public void addMakerByType(TypeMaker t,int id) {
		Random r = new Random();
		DAOMakers makersDAO = new DAOMakers();
		List<Makers> aguas = makersDAO.getMakersByType(t);
		if(aguas.size() < 1)System.out.println("No quedan casillas de "+t+". No te colocamos ninguna crack");
		else {
			Corporations c = this.find(id);
			Makers casillas = aguas.get(r.nextInt(aguas.size()));
			casillas.setCorporation(c);			
			System.out.println("Añadimos la casilla "+casillas+" a la corporacion "+c.getName()+".");
			makersDAO.update(casillas);
		}
	}
	public List<Corporations> getCorpWithOutPlayers() {
		List<Corporations> corporacionesSinJugadores = this.findAll();
		List<Corporations> corpWithOutPlayer = new ArrayList<Corporations>();
		for (Corporations corporations : corporacionesSinJugadores) {
			if(corporations.getPlayers() == null)corpWithOutPlayer.add(corporations);
		}
		return corpWithOutPlayer;
	}
	public List<Corporations> getCorpWithPlayers() {
		List<Corporations> corporacionesSinJugadores = this.findAll();
		List<Corporations> corpWithPlayer = new ArrayList<Corporations>();
		for (Corporations corporations : corporacionesSinJugadores) {
			if(corporations.getPlayers() != null)corpWithPlayer.add(corporations);
		}
		return corpWithPlayer;
	}

}
