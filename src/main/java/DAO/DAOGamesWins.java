package DAO;

import Models.Games;
import Models.GamesWins;
import Models.Players;

public class DAOGamesWins extends DAOGeneric<GamesWins, Integer>{
	public DAOGamesWins() {
		super(GamesWins.class);
	}
	public GamesWins generateAndgetGames(Games idGame, Players idPlayer) {
		GamesWins g = new GamesWins(idGame,idPlayer);
		this.save(g);
		return this.find(g.getId());
	}

}
